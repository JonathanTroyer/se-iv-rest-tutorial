using System.Collections.Generic;
using System.Linq;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;
using TodoApi.Models;

namespace TodoApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class StudentController : Controller
    {
        private const string ConnectionString = "Server=localhost;Database=StudentDB;user=root;SslMode=none;";
        private const string SaleCoConnectionString = "Server=localhost;Database=SaleCo;user=root;SslMode=none;";
        
        [HttpGet]
        public ActionResult<List<Student>> GetAll()
        {
            const string sql = "SELECT * FROM student";
            using (var conn = new MySqlConnection(ConnectionString))
            {
                return conn.Query<Student>(sql).ToList();
            }
        }

        [HttpGet("{id}", Name = "GetStudent")]
        public ActionResult<Student> GetById(string id)
        {
            const string sql = "SELECT * FROM student WHERE Id = @id";
            using (var conn = new MySqlConnection(ConnectionString))
            {
                var item = conn.QuerySingle<Student>(sql, new {id});
                if (item == null)
                    return NotFound();

                return item;
            }
        }

        [HttpGet("range")]
        public ActionResult<List<float>> GetRange()
        {
            const string highestSql = "SELECT Max(GPA) FROM student";
            const string lowestSql = "SELECT Min(GPA) FROM student";
            using (var conn = new MySqlConnection(ConnectionString))
            {
                var highest = conn.QuerySingle<float>(highestSql);
                var lowest = conn.QuerySingle<float>(lowestSql);

                return new List<float> {highest, lowest};
            }
        }

        [HttpPost]
        public ActionResult Add(Student newStudent)
        {
            const string sql = "INSERT INTO student VALUES (@Id, @Name, @GPA)";
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Execute(sql, new {newStudent.Id, newStudent.Name, newStudent.GPA});

                return CreatedAtRoute("GetStudent", new {newStudent.Id}, newStudent);
            }
        }

        [HttpDelete("{id}")]
        public ActionResult Delete(string id)
        {
            const string sql = "DELETE FROM student WHERE Id = @id";
            using (var conn = new MySqlConnection(ConnectionString))
            {
                conn.Execute(sql, new {id});
                return NoContent();
            }
        }

        // HW 4, Joins
        [HttpGet("inv")]
        public ActionResult<List<Invoice>> GetInvoices()
        {
            const string sql = @"SELECT invoice.INV_NUMBER, INV_DATE, LINE_UNITS, P_CODE, LINE_PRICE 
                FROM invoice 
                LEFT JOIN line ON line.INV_NUMBER = invoice.INV_NUMBER";
            return GetInvoicesInternal(sql);
        }

        [HttpGet("cusInv")]
        public ActionResult<List<Customer>> GetCustomers()
        {
            const string customerSql = "SELECT CUS_CODE, CUS_FNAME, CUS_LNAME FROM customer";
            var customers = GetCustomersInternal(customerSql);

            const string customerInvoiceSql = @"SELECT invoice.INV_NUMBER, INV_DATE, LINE_UNITS, P_CODE, LINE_PRICE 
                FROM invoice 
                LEFT JOIN line ON line.INV_NUMBER = invoice.INV_NUMBER
                WHERE invoice.CUS_CODE = @CusCode";
            customers.ForEach(c => { c.Invoices = GetInvoicesInternal(customerInvoiceSql, new {c.CusCode}); });


            return customers;
        }

        private static List<Customer> GetCustomersInternal(string sql)
        {
            using (var conn = new MySqlConnection(SaleCoConnectionString))
            {
                return conn.Query<Customer>(sql).ToList();
            }
        }

        private static List<Invoice> GetInvoicesInternal(string sql, object sqlArgs = null)
        {
            using (var conn = new MySqlConnection(SaleCoConnectionString))
            {
                var invoiceDict = new Dictionary<int, Invoice>();
                return conn.Query<Invoice, LineItem, Invoice>(
                        sql,
                        (invoice, lineItem) =>
                        {
                            if (!invoiceDict.TryGetValue(invoice.InvNumber, out var entry))
                            {
                                entry = invoice;
                                invoiceDict.Add(entry.InvNumber, entry);
                            }

                            entry.LineItems.Add(lineItem);
                            return entry;
                        },
                        sqlArgs,
                        splitOn: "LINE_UNITS"
                    )
                    .Distinct()
                    .ToList();
            }
        }
    }
}