namespace TodoApi.Models
{
    public class Student
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public float GPA { get; set; }

        public override string ToString()
        {
            return $"{Id},{Name},{GPA}";
        }
    }
}