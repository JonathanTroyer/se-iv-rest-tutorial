using System;
using System.Collections.Generic;

namespace TodoApi.Models
{
    public class Invoice
    {
        public int InvNumber { get; set; }
        public DateTime InvDate { get; set; }
        
        public List<LineItem> LineItems { get; } = new List<LineItem>();
    }
}