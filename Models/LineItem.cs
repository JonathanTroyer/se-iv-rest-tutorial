namespace TodoApi.Models
{
    public class LineItem
    {
        public int LineUnits { get; set; }
        public string PCode { get; set; }
        public decimal LinePrice { get; set; }
    }
}