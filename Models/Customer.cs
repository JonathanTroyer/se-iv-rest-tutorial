using System.Collections.Generic;

namespace TodoApi.Models
{
    public class Customer
    {
        
        public int CusCode { get; set; }
        public string CusFname { get; set; }
        public string CusLname { get; set; }
        
        public List<Invoice> Invoices { get; set; } = new List<Invoice>();
    }
}