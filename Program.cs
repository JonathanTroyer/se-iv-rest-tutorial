﻿using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;

/**
 * Authors - Jonathan Troyer & Chris Cavalier
 */

namespace TodoApi
{
    public static class Program
    {
        public static void Main(string[] args)
        {
            CreateWebHostBuilder(args).Build().Run();
        }

        private static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>();
    }
}